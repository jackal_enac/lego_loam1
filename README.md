# LeGO-LOAM

This package was specially made to be used with jackal in a gazebo environment, it uses lego_loam to draw a map and localize the jackal in it and record the necessary topics that can be used later to draw the map again or get the robot position at each second.


## Dependency

- [ROS](http://wiki.ros.org/ROS/Installation) (tested with noetic)
- [gtsam](https://github.com/borglab/gtsam/releases) (Georgia Tech Smoothing and Mapping library, 4.0.0-alpha2)
  ```
  wget -O ~/Downloads/gtsam.zip https://github.com/borglab/gtsam/archive/4.0.0-alpha2.zip
  cd ~/Downloads/ && unzip gtsam.zip -d ~/Downloads/
  cd ~/Downloads/gtsam-4.0.0-alpha2/
  mkdir build && cd build
  cmake ..
  sudo make install
  ```

## Compile

You can use the following commands to download and compile the package.

```
cd ~/catkin_ws/src
git clone 
cd ..
catkin_make -j1
```
When you compile the code for the first time, you need to add "-j1" behind "catkin_make" for generating some message types. "-j1" is not needed for future compiling.

## About Lego_loam

LeGO-LOAM is speficifally optimized for a horizontally placed VLP-16 on a ground vehicle. It assumes there is always a ground plane in the scan. The UGV we are using is Clearpath Jackal. It has a built-in IMU. 

The package performs segmentation before feature extraction.

Lidar odometry performs two-step Levenberg Marquardt optimization to get 6D transformation.



## Run the package

1. Run the launch file:
```
roslaunch lego_loam launch.launch
```

2. Play existing bag files:

all the bags are saved in lego_loam/LEGO_LOAM/resources/bags

to play them again : 
```
roscd lego_loam
cd resources/bags
rosbag play *.bag 
roslaunch lego_loam run.launch
```


